#ifndef WEKTOR3D_HH
#define WEKTOR3D_HH

#include <iostream>
#include <iomanip>
#include <math.h>
#include "lacze_do_gnuplota.hh"

class Wektor3D
{
    double x, y, z;//(x,y,z)
public:
    Wektor3D(double pos_x, double pos_y, double pos_z);
    Wektor3D() = default;
    void Znormalizuj(); //wektor jednostkowy
    double Norma() const;
    Wektor3D& operator+=(const Wektor3D& rhs);
    Wektor3D operator-(const Wektor3D& rhs);
    Wektor3D operator*(double liczba);
    Wektor3D operator/(double liczba);
    friend std::ostream& operator<<(std::ostream& strumien, const Wektor3D& wektor);
    friend class Dron;
};

std::ostream& operator<<(std::ostream& strumien, const Wektor3D& wektor);


#endif
