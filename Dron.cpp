#include "Dron.hh"

Wektor3D Dron::normalny()
{
        Wektor3D wf, WektorAB, WektorAD;
        WektorAB = wierzcholek[1] - wierzcholek[0];
        WektorAD = wierzcholek[3] - wierzcholek[0];
        wf.x = WektorAB.y * WektorAD.z - WektorAB.y * WektorAD.z;
        wf.y = WektorAD.x * WektorAB.z - WektorAB.x * WektorAB.z;
        wf.z = WektorAB.x * WektorAD.y - WektorAD.x * WektorAB.y;
        return wf;
}
Dron::Dron()
    {
        wierzcholek[0] = Wektor3D(30, 30, -70);
        wierzcholek[1] = Wektor3D(30,30,-50);
        wierzcholek[2] = Wektor3D(50,30,-50);
        wierzcholek[3] = Wektor3D(50,30,-70);

        wierzcholek[4] = Wektor3D(50,50,-70);
        wierzcholek[5] = Wektor3D(50,50,-50);
        wierzcholek[6] = Wektor3D(30,50,-50);
        wierzcholek[7] = Wektor3D(30,50,-70);

        ZapiszDoPliku();
    }
void Dron::PrzesunWprost(double odleglosc)
{
        Wektor3D przesuniecie = (normalny() / normalny().Norma()) * odleglosc;
        for(int i=0; i<8;i++)
        {
            wierzcholek[i]+=przesuniecie;
        }
}
void Dron::UstawKatOpadania(float kat)
    {
        kat=kat*(M_PI/180);
        Wektor3D kopiawierzcholek[8];
        for(int i=0; i<8;i++)
        {
            kopiawierzcholek[i]=wierzcholek[i];
            wierzcholek[i].z=wierzcholek[i].y * sin(kat) + wierzcholek[i].z * cos(kat);
            wierzcholek[i].y=wierzcholek[i].y * cos(kat) - sin(kat)*kopiawierzcholek[i].z;
        }
    }
void Dron::UstawKatObrotu(float kat)
    {
       kat=kat*(M_PI/180);
       Wektor3D kopiawierzcholek[8];
       for(int i=0; i<8;i++)
        {
            kopiawierzcholek[i]=wierzcholek[i];
            wierzcholek[i].x=wierzcholek[i].x * cos(kat) - wierzcholek[i].y * sin(kat);
            wierzcholek[i].y=wierzcholek[i].y * (cos(kat)) + kopiawierzcholek[i].x * sin(kat);
        }
    }
void Dron::ZapiszDoPliku()
    {
        std::fstream plik("dat/dron.pow",std::ios::out);

        const int numerki[20] = {
            0, 1, 2, 3,
            0, 7, 4, 3,
            2, 5, 4, 3,
            2, 5, 6, 1,
            0, 7, 6, 1 };

        for(int i = 0; i < 20; i++)
        {
            plik << wierzcholek[numerki[i]] << "\n";

            if(i % 4 == 3)
            {
                plik << "\n";
            }
        }

        plik.close();
    }
