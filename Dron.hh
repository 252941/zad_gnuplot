#ifndef MACIERZ_HH
#define MACIERZ_HH

#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include "lacze_do_gnuplota.hh"
#include "Wektor3D.hh"

class Dron
{
    Wektor3D wierzcholek[8];
    public:
    Wektor3D normalny();// wskazuje przód
    Dron();
    void PrzesunWprost(double odleglosc); // dodajemy do kazdego wierzcholka normalny/normalny.Norma() * dlugosc;
    void UstawKatOpadania(float kat);
    void UstawKatObrotu(float kat);
    void ZapiszDoPliku(); // zapisuje wierzchołki
};

#endif
