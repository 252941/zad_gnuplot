#include "Wektor3D.hh"
#include "lacze_do_gnuplota.hh"
Wektor3D::Wektor3D(double pos_x, double pos_y, double pos_z)
    {
        x = pos_x;
        y = pos_y;
        z = pos_z;
    }
void Wektor3D::Znormalizuj()
    {
        x=x/Norma();
        y=y/Norma();
        z=z/Norma();
    }
double Wektor3D::Norma() const
    {
        return sqrt(x*x+y*y+z*z);
    }
Wektor3D& Wektor3D::operator+=(const Wektor3D& rhs)
    {
        x+=rhs.x;
        y+=rhs.y;
        z+=rhs.z;
        return *this;
    }
Wektor3D Wektor3D::operator-(const Wektor3D& rhs)
    {
        Wektor3D wynik = *this;
        wynik.x-=rhs.x;
        wynik.y-=rhs.y;
        wynik.z-=rhs.z;
        return wynik;
    }
Wektor3D Wektor3D::operator*(double liczba)
    {
        Wektor3D wynik = *this;
        wynik.x*=liczba;
        wynik.y*=liczba;
        wynik.z*=liczba;
        return wynik;
    }
Wektor3D Wektor3D::operator/(double liczba)
    {
        Wektor3D wynik = *this;
        wynik.x/=liczba;
        wynik.y/=liczba;
        wynik.z/=liczba;
        return wynik;
    }
std::ostream& operator<<(std::ostream& strumien, const Wektor3D& wektor)
{
    strumien << wektor.x << " " << wektor.y << " " << wektor.z;
    return strumien;
}
